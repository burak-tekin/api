<?php namespace App\Util;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class API
{
    /**
     * @var bool
     */
    private $success;

    /**
     * @var int|null
     */
    private $status;

    /**
     * @var null|string
     */
    private $message;

    private function __construct($success, $status)
    {

        $this->success = (boolean)$success;
        $this->status = $status;

    }

    /**
     * Success Response
     *
     * @param int|null $status
     *
     * @return static
     */
    public static function success(?int $status =  Response::HTTP_OK): API
    {
        return new static(true, $status);
    }

    /**
     * Fail Response
     *
     * @param int|null $status
     *
     * @return static
     */
    public static function fail(?int $status = Response::HTTP_NOT_ACCEPTABLE): API
    {
        return new static(false, $status);
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    public function message(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @param array|null $data
     *
     * @return JsonResponse
     */
    public function response($data = null): JsonResponse
    {
        return new JsonResponse(
            [
                'success' => $this->success,
                'status'  => $this->status,
                'message' => $this->message,
                'data'    => $data,
            ], $this->status
        );
    }
}
