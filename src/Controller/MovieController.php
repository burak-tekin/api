<?php

namespace App\Controller;

use App\Controller\Base\IAuthentication;
use App\Repository\MovieRepository;
use App\Util\API;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MovieController
 *
 * @package App\Controller
 * @Route("/api", name="default_api")
 */
class MovieController implements IAuthentication
{
    /** @var MovieRepository */
    private $movieRepository;

    public function __construct(MovieRepository $movieRepository)
    {
        $this->movieRepository = $movieRepository;
    }

    public function isRequireAuth(): bool
    {
        return true;
    }

    /**
     * @Route("/movie", name="movie-list", methods={"GET"})
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $movies = $this->movieRepository->findBy([], [], 12);
        return API::success()->response($movies);
    }

    /**
     * @Route("/movie/{id}", name="movie-detail", methods={"GET"})
     * @param $id
     *
     * @return JsonResponse
     */
    public function get($id): JsonResponse
    {
        $movie = $this->movieRepository->findOneBy(['id' => $id]);
        if (!$movie) {
            throw new NotFoundHttpException("Movie not found.");
        }
        return API::success()->response($movie);
    }
}
