<?php

namespace App\Controller;

use App\Controller\Base\IAuthentication;
use App\Repository\MovieRepository;
use App\Util\API;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MovieSearchController
 *
 * @package App\Controller
 * @Route("/api", name="default_api")
 */
class MovieSearchController implements IAuthentication
{
    /** @var MovieRepository */
    private $movieRepository;

    public function __construct(MovieRepository $movieRepository)
    {
        $this->movieRepository = $movieRepository;
    }

    public function isRequireAuth(): bool
    {
        return true;
    }

    /**
     * @Route("/movie-search", name="movie-searchh", methods={"GET"})
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $query = strip_tags($request->get('query', "null"));
        $query = preg_replace("/[^A-Za-z0-9 ]/", "", $query);
        if (!$query || strlen($query) < 3) {
            throw new HttpException(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                "The query parameter is required and must be at least 3 characters."
            );
        }
        $movies = $this->movieRepository->createQueryBuilder('m')
            ->where(
                'MATCH_AGAINST(m.name,m.summary,m.genres) AGAINST(:searchterm boolean) > 0'
            )
            ->setParameter('searchterm', "*".$query."*")
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();

        return API::success()->response($movies);
    }
}
