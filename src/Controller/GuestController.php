<?php
namespace App\Controller;

use App\Entity\Guest;
use App\Repository\GuestRepository;
use App\Util\API;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GuestController
 *
 * @package App\Controller
 * @Route("/api", name="default_api")
 */
class GuestController extends AbstractController
{
    /** @var GuestRepository $guestRepository */
    private $guestRepository;

    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    public function __construct(GuestRepository $guestRepository, EntityManagerInterface $entityManager)
    {
        $this->guestRepository = $guestRepository;
        $this->entityManager = $entityManager;

    }

    /**
     * @Route("/token", name="aut", methods={"POST"})
     * @return JsonResponse
     */
    public function generateToken(): JsonResponse
    {
        /*
            NOT:: Normalde jwt token, refresh token olan normal authentication yapısı kullanmayı düşünüyordum
            ama tekrar case'i okuduğumda guest için basit bir token yeterli olacak diye düşündüm.
        */
        $guest = new Guest();
        $token = base64_encode(random_bytes(64));
        $guest->setToken($token);
        $this->entityManager->persist($guest);
        $this->entityManager->flush();
        return API::success(Response::HTTP_CREATED)->response(['token' => $guest->getToken()]);
    }
}
