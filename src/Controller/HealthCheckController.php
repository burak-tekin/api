<?php

namespace App\Controller;

use App\Util\API;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 *
 * @package App\Controller
 * @Route("/api", name="default_api")
 */
class HealthCheckController
{
    /**
     * @Route("/", name="health-check")
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return API::success()->response(['health' => true]);
    }
}
