<?php
namespace App\Controller\Base;

interface IAuthentication {
    public function isRequireAuth(): bool;
}
