<?php

namespace App\Service\TvMaze;

use App\Service\TvMaze\Base\IMovie;

class MovieMapper
{
    /**
     * @param array $movieResponse
     *
     * @return null|IMovie
     */
    public static function map(array $movieResponse): ?IMovie
    {
        if (isset($movieResponse['name']) && $movieResponse['name']) {
            $movie = new Movie();
            $movie->setId($movieResponse['id']);
            $movie->setGenres($movieResponse['genres']);
            $movie->setName($movieResponse['name']);
            $movie->setSummary($movieResponse['summary']);
            $movie->setImage($movieResponse['image']['original']);
            $movie->setMediumImage($movieResponse['image']['medium']);
            $movie->setRating($movieResponse['rating']['average'] ?? null);
            return $movie;
        }
        return null;
    }
}

