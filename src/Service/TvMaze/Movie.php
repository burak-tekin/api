<?php

namespace App\Service\TvMaze;

use App\Service\TvMaze\Base\IMovie;

class Movie implements IMovie
{
    /** @var int */
    private $id;
    /** @var string */
    private $name;
    /** @var string */
    private $summary;
    /** @var array */
    private $genres;
    /** @var float|null */
    private $rating;
    /** @var string|null */
    private $image;
    /** @var string|null */
    private $mediumImage;

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }


    /**
     * @return array
     */
    public function getGenres(): array
    {
        return $this->genres;
    }

    /**
     * @param array $genres
     */
    public function setGenres(array $genres): void
    {
        $this->genres = $genres;
    }

    /**
     * @param string|null $mediumImage
     */
    public function setMediumImage(?string $mediumImage): void
    {
        $this->mediumImage = $mediumImage;
    }

    /**
     * @return string|null
     */
    public function getMediumImage(): ?string
    {
        return $this->mediumImage;
    }

    /**
     * @param string|null $image
     */
    public function setImage(?string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param null|float $rating
     */
    public function setRating(?float $rating): void
    {
        $this->rating = $rating;
    }

    /**
     * @return float|null
     */
    public function getRating(): ?float
    {
        return $this->rating;
    }

    /**
     * @return string
     */
    public function getSummary(): string
    {
        return $this->summary;
    }

    /**
     * @param string $summary
     */
    public function setSummary(string $summary): void
    {
        $this->summary = $summary;
    }
}

