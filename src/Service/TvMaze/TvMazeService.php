<?php

namespace App\Service\TvMaze;

use App\Service\TvMaze\Exception\TvMazeServiceException;
use App\Service\TvMaze\Base\IMovie;
use App\Service\TvMaze\Base\ITvMazeService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class TvMazeService implements ITvMazeService
{
    const API_URL = 'https://api.tvmaze.com';

    /** @var HttpClientInterface $client */
    private $client;

    /** @var MovieMapper $movieMapper */
    private $movieMapper;

    public function __construct(HttpClientInterface $client, MovieMapper $movieMapper)
    {
        $this->client = $client;
        $this->movieMapper = $movieMapper;
    }

    /**
     * @param int|null $page
     *
     * @return array|[]IMovie
     * @throws TvMazeServiceException
     */
    public function getAllByPage(?int $page = 1): array
    {
        $movies = $this->fetch('/shows?page=' . $page);

        $result = [];
        foreach ($movies as $movie) {
            $movieMap = $this->movieMapper->map($movie);
            if ($movieMap instanceof IMovie) $result[] = $movieMap;
        }

        return $result;
    }


    /**
     * @param string $url
     *
     * @return array
     * @throws TvMazeServiceException
     */
    private function fetch(string $url): array
    {
        try {
            $response = $this->client->request(
                'GET',
                self::API_URL . $url
            );
            if ($response->getStatusCode() !== Response::HTTP_OK) {
                throw new TvMazeServiceException();
            }
            return $response->toArray();
        } catch (\Throwable $e) {
            throw new TvMazeServiceException($e->getMessage());
        }
    }
}
