<?php
namespace App\Service\TvMaze\Base;

interface IMovie
{
    /**
     * @param int $id
     */
    public function setId(int $id):void;

    /**
     * @return int
     */
    public function getId():int;

    /**
     * @param string $name
     */
    public function setName(string $name):void;

    /**
     * @return string
     */
    public function getName():string;

    /**
     * @return array
     */
    public function getGenres():array;

    /**
     * @param array $genres
     */
    public function setGenres(array $genres):void;

    /**
     * @param string|null $mediumImage
     */
    public function setMediumImage(?string $mediumImage):void;

    /**
     * @return string|null
     */
    public function getMediumImage():?string;

    /**
     * @param string|null $image
     */
    public function setImage(?string $image):void;

    /**
     * @return string|null
     */
    public function getImage():?string;

    /**
     * @param null|float $rating
     */
    public function setRating(?float $rating):void;

    /**
     * @return float|null
     */
    public function getRating():?float;
}

