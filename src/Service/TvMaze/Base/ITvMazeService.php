<?php

namespace App\Service\TvMaze\Base;

interface ITvMazeService
{
    /**
     * @param int|null $page
     *
     * @return array|[]IMovie
     */
    public function getAllByPage(?int $page = 1): array;
}
