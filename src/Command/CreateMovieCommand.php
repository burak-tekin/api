<?php

namespace App\Command;

use App\Entity\Movie;
use App\Repository\MovieRepository;
use App\Service\TvMaze\Base\IMovie;
use App\Service\TvMaze\Base\ITvMazeService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateMovieCommand extends Command
{
    protected static $defaultName = 'app:create-movie';

    const LIMIT = "limit";

    /** @var MovieRepository */
    private $movieRepository;

    /** @var ITvMazeService */
    private $tvMovieService;

    /** @var EntityManager */
    private $entityManager;

    public function __construct(MovieRepository $movieRepository,
        ITvMazeService $tvMazeService, EntityManagerInterface $entityManager
    ) {
        $this->movieRepository = $movieRepository;
        $this->tvMovieService = $tvMazeService;
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument(
            self::LIMIT, InputArgument::REQUIRED,
            'The number of movies to be created.'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$output instanceof ConsoleOutputInterface) {
            throw new \LogicException(
                'This command accepts only an instance of "ConsoleOutputInterface".'
            );
        }

        $limit = (int)$input->getArgument(self::LIMIT);
        if ($limit <= 0) {
            throw new \LogicException('Limit must be greater than zero.');
        }
        $output->writeln(
            [
                'Movie Creator',
                '============',
            ]
        );

        $output->writeln('Limit: ' . $limit);
        $output->writeln('============');

        $page = 1;
        $addedMovies = 0;
        while ($addedMovies < $limit) {
            $movies = $this->tvMovieService->getAllByPage($page);
            foreach ($movies as $movieDto) {
                if ($addedMovies >= $limit) {
                    break;
                }
                /** @var IMovie $movieDto */
                if (!$this->movieRepository->findBy(['tvMazeId' => $movieDto->getId()]))
                {
                    $movie = new Movie();
                    $movie->setName($movieDto->getName());
                    $movie->setTvMazeId($movieDto->getId());
                    $movie->setMediumImage($movieDto->getMediumImage());
                    $movie->setImage($movieDto->getImage());
                    $movie->setRating($movieDto->getRating());
                    $movie->setSummary($movieDto->getSummary());
                    $movie->setGenres($movieDto->getGenres());
                    $this->entityManager->persist($movie);
                    $this->entityManager->flush();
                    $output->writeln($movie->getName() . ' added.');
                    $addedMovies++;
                }
            }
            $page++;
        }

        $output->writeln('============');
        $output->writeln('Added Movie Count: ' . $addedMovies);
        $output->writeln('============');
        $output->writeln('FINISH');

        return Command::SUCCESS;
    }
}
