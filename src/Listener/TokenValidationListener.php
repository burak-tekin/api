<?php

namespace App\Listener;

use App\Controller\Base\IAuthentication;
use App\Repository\GuestRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;

class TokenValidationListener
{
    /** @var GuestRepository $guestRepository */
    private $guestRepository;

    public function __construct(GuestRepository $guestRepository)
    {
        $this->guestRepository = $guestRepository;
    }

    public function onKernelController(ControllerEvent $event) {
        $controller = $event->getController();
        if (is_array($controller)) {
            $controller = $controller[0];
        }
        if ($controller instanceof IAuthentication && $controller->isRequireAuth()){
            $token = $event->getRequest()->headers->get('Authorization', null);
            if (is_null($token)) $this->tokenValidationFail();
            $token = ltrim($token,"Bearer ");
            if (!$this->guestRepository->findOneBy(['token' => $token])) $this->tokenValidationFail();
        }
    }


    public function tokenValidationFail(): void
    {
         throw new HttpException(Response::HTTP_UNAUTHORIZED,'This action needs a valid token!');
    }
}
